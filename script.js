//form submission
const form = document.getElementById("form");
form.addEventListener('submit',handleSubmission);

function handleSubmission(event){
  event.preventDefault()
  


//get picture,name,phone from input feilds
const pictureInput = form['picture'];
const picture = pictureInput.value;
const nameInput = form['name'];
const name = nameInput.value;
const phoneInput = form['phone'];
const phone = phoneInput.value;
const contactlist = document.getElementById('contactList');

//form validation
if(!picture){
  alert("picture is required");
  return;
}
else if(!name){
  console.log("name is required");
  return
}
else if(!phone){
  console("phone number is required");
  return;
}

//name must be less than 20 charecter
if (name.length >20){
  console.log("name must be less than 20 charecter");
  return;
}
if (phone.length !==10){
 console.log("phone number must be 10 digit long");
 return;
}
const regExp = /^(\+\d{1,3}\s?)?(\(\d{1,4}\)\s?)?[\d\s\-]+$/;
if(!regExp.test(phone)){
 console.log("phone number is invalid");
 return;
}
// create contact
const li = document.createElement('li');
const img = document.createElement('img');
img.src = picture;
li.appendChild(img);

// display contact
const division = document.createElement('div');
li.appendChild(division);
const h3 = document.createElement('h3');
h3.innerHTML = name;
division.appendChild(h3);
const span = document.createElement('span');
span.innerHTML = phone;
division.appendChild(span);
li.appendChild(division);
contactlist.appendChild(li);


}